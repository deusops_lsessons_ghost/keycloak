# Установка keycloak в режиме ha

В результате разворачиваются две ноды keycloak (версия 20.0.3 quarkus) в режиме high availability (общий кеш) за loadbalancer (nginx, получаем для него letsencrypt сертификат) с базой данных (postgresql), так же в keycloak создается тестовый realm.

## Инфраструктура

VM для проекта разворачиваются с помощью terraform. Для этого необходимо заполнить [terraform.tfvars.example](https://gitlab.com/deusops_lsessons_ghost/keycloak/-/blob/master/terraform/terraform.tfvars.example), переименовать его => **terraform.tfvars** и [backend.conf.example](https://gitlab.com/deusops_lsessons_ghost/keycloak/-/blob/master/terraform/backend.conf.example), переименовать в => **backend.conf**, настройки можно изменить в [00-vars.tf](https://gitlab.com/deusops_lsessons_ghost/keycloak/-/blob/master/terraform/00-vars.tf) и выполнить последовательно две команды:
```
$ terraform init -backend-config=backend.conf

$ terraform apply -auto-approve
```
Далее нужно скачать роли для ansible и запустить [playbook](https://gitlab.com/deusops_lsessons_ghost/keycloak/-/blob/master/playbook.yml) (файл hosts ([пример hosts](https://gitlab.com/deusops_lsessons_ghost/keycloak/-/blob/master/environments/dev/example.hosts)) для ansible генерируется с помощью terraform):
```
$ ansible-galaxy install -r requirements.yml

$ ansible-playbook playbook.yml
```
Роли лежат [отдельно](https://gitlab.com/ghosta_projects/ansible_roles). Все роли протестированы с помощью molecule 4.0.4


## Зависимости

Можно установить без БД и обратного прокси сервера (standalone режим)
