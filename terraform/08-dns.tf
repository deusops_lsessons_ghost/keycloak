resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "Test public zone"

  labels = {
    label1 = "base.domain"
  }

  zone   = "${var.domain-name}."
  public = true
}

resource "yandex_dns_recordset" "balancer" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "${var.domain-name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.node["balancer"].network_interface.0.nat_ip_address]
}
