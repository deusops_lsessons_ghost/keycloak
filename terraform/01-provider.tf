terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  # access_key and secret_key are in backend.conf => terraform init -backend-config=backend.conf
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "deusops-lessons"
    region     = "ru-central1-a"
    key        = "terraform/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }

  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yandex-token
  cloud_id  = var.yandex-cloud-id
  folder_id = var.yandex-folder-id
  zone      = var.zone
}
