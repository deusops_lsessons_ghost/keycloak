output "external_ips" {
  value = {
    for instance in yandex_compute_instance.node :
    instance.name => yandex_compute_instance.node[instance.name].network_interface.0.nat_ip_address
  }
}

resource "local_file" "ansible_hosts" {
  content = templatefile("./template/hosts.tftpl", {
    nodes = {
      for key, values in local.virtual_machines :
        key => {
          ip = yandex_compute_instance.node[key].network_interface.0.nat_ip_address,
        }
    },
    domain_name           = var.domain-name,
    vps_subnet            = yandex_vpc_subnet.subnet_terraform.v4_cidr_blocks[0],
    keycloak_user         = var.keycloak-user,
    keycloak_ssh_key_path = var.keycloak-ssh-key-path,
    balancer_user         = var.balancer-user,
    balancer_ssh_key_path = var.balancer-ssh-key-path,
    db_user               = var.db-user,
    db_ssh_key_path       = var.db-ssh-key-path
  })
  filename = "../environments/dev/hosts"
}
