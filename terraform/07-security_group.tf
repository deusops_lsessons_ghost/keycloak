resource "yandex_vpc_security_group" "keycloak" {
  name        = "Keycloak security group"
  description = "Description for keycloak security group"
  network_id  = yandex_vpc_network.network_terraform.id

  ingress {
    protocol       = "ICMP"
    v4_cidr_blocks = yandex_vpc_subnet.subnet_terraform.v4_cidr_blocks
  }

  ingress {
    protocol       = "TCP"
    description    = "SSH"    
    v4_cidr_blocks = [ "0.0.0.0/0" ]
    port           = 22
  }

  ingress {
    protocol       = "TCP"
    description    = "HTTP"
    v4_cidr_blocks = [ "0.0.0.0/0" ]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "HTTPS"
    v4_cidr_blocks = [ "0.0.0.0/0" ]
    port           = 443
  }

  egress {
    protocol       = "ANY"
    description    = "Allow all outgoing traffic"
    v4_cidr_blocks = yandex_vpc_subnet.subnet_terraform.v4_cidr_blocks
  }
}


